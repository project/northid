<?php

	$view = new view;
	$view->name = 'northid_notifications';
	$view->description = '';
	$view->tag = '';
	$view->view_php = '';
	$view->base_table = 'users';
	$view->is_cacheable = FALSE;
	$view->api_version = 2;
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
	$handler = $view->new_display('default', 'Defaults', 'default');
	$handler->override_option('fields', array(
	  'uid' => array(
	    'label' => 'Uid',
	    'alter' => array(
	      'alter_text' => 0,
	      'text' => '',
	      'make_link' => 0,
	      'path' => '',
	      'link_class' => '',
	      'alt' => '',
	      'prefix' => '',
	      'suffix' => '',
	      'target' => '',
	      'help' => '',
	      'trim' => 0,
	      'max_length' => '',
	      'word_boundary' => 1,
	      'ellipsis' => 1,
	      'html' => 0,
	      'strip_tags' => 0,
	    ),
	    'empty' => '',
	    'hide_empty' => 0,
	    'empty_zero' => 0,
	    'link_to_user' => 1,
	    'exclude' => 1,
	    'id' => 'uid',
	    'table' => 'users',
	    'field' => 'uid',
	    'relationship' => 'none',
	  ),
	  'nothing' => array(
	    'label' => '',
	    'alter' => array(
	      'text' => 'Add Online ID',
	      'make_link' => 1,
	      'path' => 'user/[uid]/northid_manage',
	      'link_class' => '',
	      'alt' => '',
	      'prefix' => '',
	      'suffix' => '',
	      'target' => '',
	      'help' => '',
	      'trim' => 0,
	      'max_length' => '',
	      'word_boundary' => 1,
	      'ellipsis' => 1,
	      'html' => 0,
	      'strip_tags' => 0,
	    ),
	    'empty' => '',
	    'hide_empty' => 0,
	    'empty_zero' => 0,
	    'exclude' => 0,
	    'id' => 'nothing',
	    'table' => 'views',
	    'field' => 'nothing',
	    'relationship' => 'none',
	    'override' => array(
	      'button' => 'Override',
	    ),
	  ),
	));
	$handler->override_option('filters', array(
	  'uid_current' => array(
	    'operator' => '=',
	    'value' => '1',
	    'group' => '0',
	    'exposed' => FALSE,
	    'expose' => array(
	      'operator' => FALSE,
	      'label' => '',
	    ),
	    'id' => 'uid_current',
	    'table' => 'users',
	    'field' => 'uid_current',
	    'relationship' => 'none',
	  ),
	  'uid' => array(
	    'operator' => '=',
	    'value' => '0',
	    'group' => '0',
	    'exposed' => FALSE,
	    'expose' => array(
	      'operator' => FALSE,
	      'label' => '',
	    ),
	    'id' => 'uid',
	    'table' => 'northid',
	    'field' => 'uid',
	    'relationship' => 'none',
	  ),
	  'status' => array(
	    'operator' => '=',
	    'value' => '1',
	    'group' => '0',
	    'exposed' => FALSE,
	    'expose' => array(
	      'operator' => FALSE,
	      'label' => '',
	    ),
	    'id' => 'status',
	    'table' => 'users',
	    'field' => 'status',
	    'override' => array(
	      'button' => 'Override',
	    ),
	    'relationship' => 'none',
	  ),
	));
	$handler->override_option('access', array(
	  'type' => 'none',
	));
	$handler->override_option('cache', array(
	  'type' => 'none',
	));
	$handler->override_option('title', 'Verify your Online ID');
	$handler->override_option('header', 'Add Online ID link to your account to verify your identity to online friends and readers and protect yourself from identity theft.');
	$handler->override_option('header_format', '1');
	$handler->override_option('header_empty', 0);
	$handler = $view->new_display('block', 'Missing Online ID link', 'block_1');
	$handler->override_option('block_description', '');
	$handler->override_option('block_caching', -1);
	$handler = $view->new_display('block', 'Online ID about to expire', 'block_2');
	$handler->override_option('fields', array(
	  'uid' => array(
	    'label' => 'Uid',
	    'alter' => array(
	      'alter_text' => 0,
	      'text' => '',
	      'make_link' => 0,
	      'path' => '',
	      'link_class' => '',
	      'alt' => '',
	      'prefix' => '',
	      'suffix' => '',
	      'target' => '',
	      'help' => '',
	      'trim' => 0,
	      'max_length' => '',
	      'word_boundary' => 1,
	      'ellipsis' => 1,
	      'html' => 0,
	      'strip_tags' => 0,
	    ),
	    'empty' => '',
	    'hide_empty' => 0,
	    'empty_zero' => 0,
	    'link_to_user' => 1,
	    'exclude' => 1,
	    'id' => 'uid',
	    'table' => 'users',
	    'field' => 'uid',
	    'relationship' => 'none',
	  ),
	  'nothing' => array(
	    'label' => '',
	    'alter' => array(
	      'text' => 'Extend Online ID',
	      'make_link' => 1,
	      'path' => 'user/[uid]/northid_manage',
	      'link_class' => '',
	      'alt' => '',
	      'prefix' => '',
	      'suffix' => '',
	      'target' => '',
	      'help' => '',
	      'trim' => 0,
	      'max_length' => '',
	      'word_boundary' => 1,
	      'ellipsis' => 1,
	      'html' => 0,
	      'strip_tags' => 0,
	    ),
	    'empty' => '',
	    'hide_empty' => 0,
	    'empty_zero' => 0,
	    'exclude' => 0,
	    'id' => 'nothing',
	    'table' => 'views',
	    'field' => 'nothing',
	    'relationship' => 'none',
	    'override' => array(
	      'button' => 'Use default',
	    ),
	  ),
	));
	$handler->override_option('filters', array(
	  'uid_current' => array(
	    'operator' => '=',
	    'value' => '1',
	    'group' => '0',
	    'exposed' => FALSE,
	    'expose' => array(
	      'operator' => FALSE,
	      'label' => '',
	    ),
	    'id' => 'uid_current',
	    'table' => 'users',
	    'field' => 'uid_current',
	    'relationship' => 'none',
	  ),
	  'status' => array(
	    'operator' => '=',
	    'value' => '1',
	    'group' => '0',
	    'exposed' => FALSE,
	    'expose' => array(
	      'operator' => FALSE,
	      'label' => '',
	    ),
	    'id' => 'status',
	    'table' => 'users',
	    'field' => 'status',
	    'override' => array(
	      'button' => 'Override',
	    ),
	    'relationship' => 'none',
	  ),
	  'crnvalidity' => array(
	    'operator' => '<',
	    'value' => array(
	      'type' => 'offset',
	      'value' => '+14 days',
	      'min' => '',
	      'max' => '',
	    ),
	    'group' => '0',
	    'exposed' => FALSE,
	    'expose' => array(
	      'operator' => FALSE,
	      'label' => '',
	    ),
	    'id' => 'crnvalidity',
	    'table' => 'northid',
	    'field' => 'crnvalidity',
	    'override' => array(
	      'button' => 'Use default',
	    ),
	    'relationship' => 'none',
	  ),
	));
	$handler->override_option('title', 'Online ID certificate is about to expire');
	$handler->override_option('header', 'Your Online ID certificate is about to expire in less then 14 days.');
	$handler->override_option('block_description', '');
	$handler->override_option('block_caching', -1);
