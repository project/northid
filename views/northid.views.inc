<?php

/**
 * @file
 * Views 2 hooks and callback registries.
 */


/**
 * Implementation of hook_views_data().
 */
function northid_views_data() {
	
  $data['northid']['table']['group'] = t('NorthID');

  $data['northid']['table']['base'] = array(
    'field' => 'uid',
    'title' => t('NorthID'),
    'help' => t("Accounts linked to NorthID Online ID system."),
  );

  $data['northid']['table']['join'] = array(
		'node' => array(
      'left_field' => 'uid',
      'field' => 'uid',			
		),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  $data['northid']['uid'] = array(
    'title' => t('NorthID view link'),
    'help' => t("Link to NorthID Online ID for current user."),
    'field' => array(
      'handler' => 'northid_handler_field_northid_view',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator_string',
			'accept_null' => TRUE,
			'label' => t('Has NorthID'),
			'type' => 'yes-no'
    ),
	  'sort' => array(
	    'handler' => 'views_handler_sort',
			'label' => t('Has valid NorthID'),
	  ),
  );

	$data['northid']['crnvalidity'] = array(
	  'title' => t('NorthID valid until'),
	  'help' => t("Date until NorthID certificate is valid."),
	  'field' => array(
	    'handler' => 'views_handler_field_date',
	    'click sortable' => TRUE,
	  ),
	  'filter' => array(
	    'handler' => 'views_handler_filter_date',
	  ),
	  'sort' => array(
	    'handler' => 'views_handler_sort_date',
	  ),
	);

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function northid_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'northid') .'/views',
    ),
    'handlers' => array(
      'northid_handler_field_northid_view' => array(
        'parent' => 'views_handler_field',
      ),
	    'northid_handler_field_northid_validuntil' => array(
	      'parent' => 'views_handler_field',
	    ),
    ),
  );
}
