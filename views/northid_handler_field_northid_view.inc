<?php

/**
 * @file
 * Views handler: NorthID view field.
 */

/**
 * Return a formatted price value to display in the View.
 */
class northid_handler_field_northid_view extends views_handler_field {
  function options(&$options) {
    parent::options($options);

  }

  function query() {
    $this->ensure_my_table();
    // Add the field.
    $this->field_alias = $this->query->add_field($this->table_alias, $this->real_field);

    $this->add_additional_fields();
  }

  function render($values) {
	
		if ($values->northid_uid == NULL) {
			return "";
		} elseif (user_access('view northid user link')) {
			$uid = $values->northid_uid;
			return drupal_get_form('northid_clicklink_form', $uid);
		} else {
			// no access
			return "";
		}
			
  	return parent::render($values);
  }
}

function northid_clicklink_form($form_state, $uid) {
	$form['#prefix'] = '<div id="northid-clicklink-'.$uid.'-wrapper">';
	$form['northid_click_'.$uid] = array(
		'#weight' => 2,
		'#type' => 'image_button',
		'#value' => t("NorthID"),
		'#src' => variable_get('northid_link_image', drupal_get_path('module', 'northid').'/northid_view.gif'),
		'#ahah' => array(
			'event' => 'click',
			'path' => 'northid/ajax/'.$uid,
			'wrapper' => 'northid-clicklink-'.$uid.'-wrapper',
			'method' => 'replace_submit',
		),
	);
	$form['#suffix'] = '</div>';
	return $form;
}
