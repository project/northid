<?php
function northid_views_default_views() {
  $files = file_scan_directory(drupal_get_path('module', 'northid'). '/views/defaults', '.inc', array('recurse' => FALSE));
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}